import React from 'react';
import DrawerNavigation from './Navigators/DrawerNavigation';

export default function App() {
  return (
    <DrawerNavigation/>
  );
}
