import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import Header from '../Header/Header';

export default function About({navigation}) {
    return (
        <View style={styles.container}>
            <Header title="About us" navigation={navigation} />
            <View style={styles.content}>
                <Text style={styles.text}>My name is Santanu Bhattacharya, I am a senior web developer and currently working for Continental Automotive. This is an application building with help of Udemy tutorials.</Text>
                <Button title="Go back" onPress={() => navigation.goBack()}  />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#81A7F9'
    },
    text: {
        fontSize: 20,
        padding: 40,
        color: '#ffffff',
        fontWeight: '000',
        marginBottom: 150

    }
})